# -----------------------------------------------------------------------------
# ColumnOutput
#
# Carlos M. Martinez, carlos@xt6.us 2020-10-12
#
# -----------------------------------------------------------------------------

import sys

class ColumnOutput:
    def __init__(self):
        self.rows = []
    # end constructor

    def reset(self):
        self.rows = []
    # end reset

    def addRow(self, w_row):
        self.rows.append(w_row)
    # end addRow

    def close(self):
        pass
# end class 

class ColumnCsvOutput(ColumnOutput):
    """Write output to CSV file"""
    
    def __init__(self, w_filename, **kwargs):
        """default constructor
        w_filename: nombre del archivo
        w_delimiter: delimitador del archivo CSV
        """
        super().__init__()
        self.delimiter = kwargs.get("delimiter", ",")
        self.f = open(w_filename, 'w')
        self.header_already_written = False
    # end init

    def addRow(self, w_row):
        super().addRow(w_row)
        row_to_write = []
        row_header = []
        for k, v in w_row.items():
            row_to_write.append(v)
            row_header.append(k)

        if not self.header_already_written:
            self.f.write(self.delimiter.join(row_header)+"\n")    
            self.header_already_written = True

        self.f.write(self.delimiter.join(row_to_write)+"\n")
    # end addrow

    def close(self):
        """close file descriptor"""
        super().close()
        self.f.close()
    # end close
# end ColumnCsvOutput

if __name__ == "__main__":
    print("Not to be run directly")
    sys.exit(1)

# -----------------------------------------------------------------------------