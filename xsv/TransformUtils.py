# -----------------------------------------------------------------------------
# TransformUtils
#
# Carlos M. Martinez, carlos@xt6.us 2020-10-24
#
# -----------------------------------------------------------------------------

## Funcion identidad
def identity(x):
    """returns the same value as passed"""
    return x
## end identity

## Function copy field
def copyfield(w_field, w_row):
    """Copies a field"""
    return w_row.get(w_field, None)
## end Function copy field