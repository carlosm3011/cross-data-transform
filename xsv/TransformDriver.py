# -----------------------------------------------------------------------------
# TransformDriver
#
# Carlos M. Martinez, carlos@xt6.us 2020-10-12
#
# -----------------------------------------------------------------------------

import sys

class TransformDriver:

    def __init__(self, **kwargs):
        self.ss_input = kwargs['input']
        self.ss_output = kwargs['output']
        self.tm = kwargs['transform_model']
    # end constructor

    def run(self):
        self.ss_output.reset()
        for r in self.ss_input.rows:
            nr = self.tm.transformRow(r)
            self.ss_output.addRow(nr)
        #
        print(self.ss_output.rows)
        #
        self.ss_output.close()
    # end run

if __name__ == "__main__":
    print("Not to be run directly")
    sys.exit(1)

# -----------------------------------------------------------------------------