# -----------------------------------------------------------------------------
# TransformModel
#
# Carlos M. Martinez, carlos@xt6.us 2020-10-14
#
# -----------------------------------------------------------------------------

import xsv.TransformUtils as tu 

class TransformModel:

    def __init__(self, w_target_functions):
        self.target_functions = dict(w_target_functions)
    # end constructor

    def transformRow(self, w_row):
        r_row = dict({})
        #
        field_names = set (
            list(self.target_functions.keys()) +
            list(w_row.keys())
        )
        #
        for f in field_names:
            # get the function for the field or just copy the field
            fn = self.target_functions.get(f, tu.copyfield) 
            r_row[f] = fn( f, w_row )
        #
        return r_row

    # end transformRow

# end class


if __name__ == "__main__":
    print("Not to be run directly")
    sys.exit(1)

# -----------------------------------------------------------------------------