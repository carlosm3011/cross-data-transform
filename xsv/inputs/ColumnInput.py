# -----------------------------------------------------------------------------
# ColumnInput
#
# Carlos M. Martinez, carlos@xt6.us 2020-10-12
#
# -----------------------------------------------------------------------------

import sys
import csv

class ColumnInput:

    """ Entrada en filas y columnas
    """
    # constructor
    def __init__(self, w_row_count):
        self.rows = []
        for x in range(0, w_row_count):
            self.rows.append({ 'colA': 'aaa',
                               'colB': 'bbb',
                               'colC': 'ccc',
            })
    ## end constructor

    # analyze
    def analyze(self):
        pass
    # end analyze

# end class Column Input

## CSV INPUT 
class ColumnCsvInput(ColumnInput):

    # constructor
    def __init__(self, w_file_name, **kwargs):
        self.file_name = w_file_name
        self.delimiter = kwargs.get("delimiter", ",")
        super().__init__(0)
        self.csv_file_fd = open(self.file_name, newline='')
        self.rows = csv.DictReader(self.csv_file_fd, delimiter=self.delimiter)
    # end constructor
## end class ColumnCsvInput

if __name__ == "__main__":
    print("Not to be run directly")
    sys.exit(1)

# -----------------------------------------------------------------------------