# Cross Data Transform

**Author: Carlos M. Martinez**
**Date: 2020-10-12**

CXT is a data conversion engine. Input data is conceptually like a spreadsheet (rows and columns of data cells), is transformed using a model composed of a set of functions and a CSV-like output is produced.

Possible transformations include:

- deleting a field
- adding a new field with:
   - a fixed value
   - a calculated value using the values of the input fields for a given row


## VERSION 0.1: TO-DO TDDs 

### TransformDriver
- TEST: rows must be coherent
   - Decide:
      - build with a template ? 
      - use the first row as template ?

### ColumnInput
- csv input
   - TEST: build interface, read file

### ColumnOutput
- csv output
   - TEST: write output with headers

### TransformUtils
- IP prefix conversions