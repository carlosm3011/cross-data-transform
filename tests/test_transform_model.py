
# -----------------------------------------------------------------------------
# tests transform model 
#
# Carlos M. Martinez, carlos@xt6.us 2020-10-14
#
# -----------------------------------------------------------------------------

import xsv.inputs.ColumnInput as ColumnInput
import xsv.outputs.ColumnOutput as ColumnOutput
import xsv.TransformDriver as TransformDriver
import xsv.TransformModel as TransformModel
import xsv.TransformUtils as tu

import pytest
from tests.test_main import dummy_row_counts

class TestTransformModel:

    def test_create_transform_model_01(self):
        tm = TransformModel.TransformModel({})
    # end

    def test_transform_one_row_identity_02(self):
        onerow = {'a': 'aaa', 'b': 'bbb'}
        target_functions = {'a': tu.copyfield, 'b': tu.copyfield}
        tm = TransformModel.TransformModel(target_functions)
        newrow = tm.transformRow(onerow)
        assert len(newrow.keys()) == len(onerow.keys())
        assert newrow['a'] == 'aaa'
        assert newrow['b'] == 'bbb'
    # end test transform one row

    def test_transform_one_row_add_field_03(self):
        onerow = {'a': 'aaa', 'b': 'bbb'}
        target_functions = {'a': tu.copyfield, 'b': tu.copyfield, 'c': lambda x,y:  'ccc'}
        tm = TransformModel.TransformModel(target_functions)
        newrow = tm.transformRow(onerow)
        assert len(newrow.keys()) ==  len( onerow.keys() ) + 1 
    # end test transform one row

    def test_transform_one_row_add_field_calculated_04(self):
        onerow = {'a': 'aaa', 'b': 'bbb'}
        target_functions = {'a': tu.copyfield, 'b': tu.copyfield, 'c': lambda x,y:  y['a']+y['b'] }
        tm = TransformModel.TransformModel(target_functions)
        newrow = tm.transformRow(onerow)
        assert len(newrow.keys()) ==  len( onerow.keys() ) + 1 
        #
        assert newrow['c'] == 'aaabbb'
    # end test transform one row ... 04 

    def test_transform_one_row_add_field_calculated_all_identities_but_one_05(self):
        onerow = {'a': 'aaa', 'b': 'bbb'}
        target_functions = {'c': lambda x,y:  y['a']+y['b'] }
        tm = TransformModel.TransformModel(target_functions)
        newrow = tm.transformRow(onerow)
        assert len(newrow.keys()) ==  len( onerow.keys() ) + 1 
        #
        assert newrow['a'] == 'aaa'
        assert newrow['b'] == 'bbb'
        assert newrow['c'] == 'aaabbb'
    # end test transform one row ... 05
# end class