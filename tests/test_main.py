
# -----------------------------------------------------------------------------
# tests
#
# Carlos M. Martinez, carlos@xt6.us 2020-10-12
#
# -----------------------------------------------------------------------------

import xsv.inputs.ColumnInput as ColumnInput
import xsv.outputs.ColumnOutput as ColumnOutput
import xsv.TransformDriver as TransformDriver
import xsv.TransformModel as tm

import pytest

@pytest.fixture
def dummy_row_counts():
    return 2

class TestMain:

    def test_main(self, dummy_row_counts):
        """
        Asi seria el loop main de la aplicacion. Crear un input, un output, 
        Instanciar el driver y largarlo a correr.
        """
        ss_input = ColumnInput.ColumnInput(dummy_row_counts)

        ss_output = ColumnOutput.ColumnOutput()

        xd = TransformDriver.TransformDriver(input=ss_input, 
                output=ss_output, 
                transform_model=tm.TransformModel({}) )

        xd.run()
    # end test_main

# end class TestMain

# -- END ----------------------------------------------------------------------