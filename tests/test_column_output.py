
# -----------------------------------------------------------------------------
# tests column input 
#
# Carlos M. Martinez, carlos@xt6.us 2020-10-14
#
# -----------------------------------------------------------------------------

import xsv.inputs.ColumnInput as ColumnInput
import xsv.outputs.ColumnOutput as ColumnOutput
import xsv.TransformDriver as TransformDriver
import xsv.TransformModel as TransformModel
import csv

import pytest
from tests.test_main import dummy_row_counts

@pytest.fixture
def ss_output(dummy_row_counts):
    """
    Sample Main
    """
    ss_input = ColumnInput.ColumnInput(dummy_row_counts)
    ss_output = ColumnOutput.ColumnOutput()

    xd = TransformDriver.TransformDriver(
        input = ss_input, 
        output = ss_output, 
        transform_model = TransformModel.TransformModel({'colX': lambda x,y: 'xxx'})
    )

    xd.run()
    return ss_output
## end ss_output

class TestColumnOutput:

        def test_createOutput_01(self, ss_output):
            pass
        # end test create output

        def test_output_has_rows_02(self, ss_output, dummy_row_counts):
            cnt = 0
            for r in ss_output.rows:
                cnt = cnt + 1
            # end for
            assert cnt == dummy_row_counts
        # end test output has rows

        def test_output_has_columns_03(self, ss_output, dummy_row_counts):
            assert ss_output.rows[0]['colA'] == 'aaa'
        # end test output has columns

        def test_output_has_additional_column_04(self, ss_output, dummy_row_counts):
            assert ss_output.rows[0]['colX'] == 'xxx'
            # pass
        # end test output has columns 


@pytest.fixture(params = ["|",",",";"])
def ss_csv_output(dummy_row_counts, request):
    """
    Run a transform driver with dummy input and csv output.
    """
    w_delimiter = request.param
    ss_input = ColumnInput.ColumnInput(10)
    ss_output = ColumnOutput.ColumnCsvOutput("tests/output_01.csv", delimiter=w_delimiter)

    xd = TransformDriver.TransformDriver(
        input = ss_input, 
        output = ss_output, 
        transform_model = TransformModel.TransformModel({'colX': lambda x,y: 'xxx'})
    )

    xd.run()
    return ss_output
## end ss_output

class TestColumnCsvOutput:
        def test_createCsvOutput_01(self, ss_csv_output):
            pass

        def test_output_file_exists_02(self, ss_csv_output):
            assert open("tests/output_01.csv", 'r') 

        def test_output_file_has_rows_03(self, ss_csv_output):
            fd = open("tests/output_01.csv", "r")
            lines = fd.readlines()
            assert len(lines) > 0
            fd.close()
        # end tests 03

        def test_output_file_correct_content_04(self, ss_csv_output):
            fd = open("tests/output_01.csv", "r")
            rows = csv.DictReader(fd, delimiter=ss_csv_output.delimiter)
            r = []
            for x in rows:
                r.append(x)
            print("Now testing with delimiter: [", ss_csv_output.delimiter,"]")
            print(r[2])
            fd.close()
            assert r[2]['colB'] == 'bbb'
        # end tests 03 
               

# end class test column output