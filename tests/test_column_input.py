
# -----------------------------------------------------------------------------
# tests column input 
#
# Carlos M. Martinez, carlos@xt6.us 2020-10-14
#
# -----------------------------------------------------------------------------

import xsv.inputs.ColumnInput as ColumnInput
import xsv.outputs.ColumnOutput as ColumnOutput
import xsv.TransformDriver as TransformDriver

import pytest

from tests.test_main import dummy_row_counts

@pytest.fixture(params=[
        ColumnInput.ColumnInput(2)
        ]
    )
def ss_input(request):
    return request.param
## end ss_input

t_input_files = [
    {'filename': 'tests/test_04.csv',
      'delimiter': ','},
    {'filename': 'tests/test_05.csv',
      'delimiter': ','},
    {'filename': 'tests/test_06_pipesep.csv',
      'delimiter': '|'}
]

@pytest.fixture(params=t_input_files)
def ss_csv_input(request):
    csv_input = ColumnInput.ColumnCsvInput(request.param['filename'], delimiter=request.param['delimiter'])
    return csv_input
# end ss_csv_input

def test_createInput_01(ss_input, dummy_row_counts):
    cnt = 0
    for r in ss_input.rows:
        cnt = cnt + 1 

    assert cnt == dummy_row_counts
# end test crear input

def test_create_input_read_column_02(ss_input):
    for r in ss_input.rows:
        assert r['colA'] == 'aaa'
        assert r['colB'] == 'bbb'
        assert r['colC'] == 'ccc'
# end test crear input read column

def test_raise_on_csv_file_notfound_03():
    with pytest.raises(FileNotFoundError):
        ss_csv_input = ColumnInput.ColumnCsvInput('nonexistant.csv')
# end test_raise_on_csv_file_notfound_03():

def test_count_rows_from_test_csv_04(ss_csv_input):
    n = 0
    for r in ss_csv_input.rows:
        n = n + 1
    # end for
    assert n == 2
# end test_count_rows_from_test_csv_04(ss_csv_input):

def test_read_rows_from_test_csv_05(ss_csv_input):
    print("filename: ", ss_csv_input.file_name)
    for r in ss_csv_input.rows:
        print(r)
        assert r['colA'] == 'aaa'
        assert r['colB'] == 'bbb'
        assert r['colC'] == 'ccc'
# end test_read_rows_from_test_csv_5(ss_csv_input):

# end class test column input